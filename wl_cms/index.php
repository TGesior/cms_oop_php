<?php include("app/init.php") ?>
<!DOCTYPE html>


    <head>
        <title>wlasnycms</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="app/res/css/sk_style.css" />
        <?php $SK->head(); ?>
    </head>
    <body>
        <?php $SK->toolbar(); ?>
        <div id="wrapper">
        
            <div class="head-left">
                <h1>WLASNYCMS</h1>
            </div>
            
            <div class="head-right">
                <?php $SK->login_link(); ?>
            </div>
            <div class="clear">
                
            </div>
            <div class="navbar">
                <div class="navbar-inner">
                    <ul class="nav">
                        <li><a href="#">START</a></li>
                        <li><a href="#">link</a></li>
                        <li><a href="#">O nas</a></li>
                        <li><a href="#">Kontakt</a></li>
                    </ul>
                </div>
            </div>
            
            <div class="content row">
                <div class="span9" id="left-c">
                    <h2><?php $SK->Cms->display_block('content-header','oneline'); ?></h2>
                    <hr />
                    <div class="well">
                        <?php $SK->Cms->display_block('content-main'); ?>
                    </div>
                </div> 
                <div class="span3" id="right-c">
                    <div class="well right-content">
                        <?php $SK->Cms->display_block('content-right-first','textarea'); ?>
                    </div>
                    <div class="well right-content">
                        <?php $SK->Cms->display_block('content-right-second','textarea'); ?>
                    </div>
                    <div>
                        <img src="<?php SITE_PATH ?>app/res/images/right_cms.png" alt="" />
                    </div> 
                </div>
            </div> 
            <div id="footer">
                &copy; 2016 Tomasz Ge
            </div>
        </div>
    </body>
